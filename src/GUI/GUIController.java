/*
 * Copyright (C) 2014 Bonafide
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package GUI;

import Copying.BuildAndCopy;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.WARNING_MESSAGE;


/**
 *
 * @author Bonafide
 */
public class GUIController implements Initializable  {

    
    //Directory chooser
    final DirectoryChooser chooser = new DirectoryChooser();
    
    //Strings to hold the source and target path
    String source = "null";
    String target = "null";
    
    //Copy worker Task to determine when all threads are finished.
    Task copyWorker;

    // Source Text Field on GUI
    @FXML
    TextField srcText;
    
    //Target Text Field on GUI
    @FXML
    TextField tarText;
    
    @FXML
    Label progress;
    
    @FXML
    ProgressBar progressBar;
    
    @FXML
    private void handleBrowse(ActionEvent event) {
        
       // Get a directory from a user
        File directory = chooser.showDialog(null);
       
       
       //If the directory exists
       if(directory.exists()){
           
           
               
               // Make an object to convert later to a button
               Object object = event.getSource();
               
               //Check to see if it is a button
               if(object instanceof Button){
                   
                   //If it was make a temporary button 
                    Button temp = (Button)object;
                    
                    //if the calling button was the source browse button
                    if (temp.getId().equals("source")){
                        
                        //Set the text to that directory path to show the user it was set
                        srcText.setText(directory.getPath());
                        
                        //Set source to that directory path
                        source = directory.getPath();
                        
                    }
                    
                    // If it wasn't the source button then it has to be the target button
                    else {
                        
                        // set the text to that directory path and showe the user it was set
                        tarText.setText(directory.getPath());
                        
                        //set the target to that directory path.
                        target = directory.getPath();
                    }
                    
               
             }
       
         }
       

    }
    
    @FXML
    private void handleSync(ActionEvent event) throws InterruptedException {
        
        //Make sure that both source and target are set
        if(source != null && target != null){
            
            
           //If both source and target are set then build lists and start copying.
           BuildAndCopy thisBuild = new BuildAndCopy(source,target);
           thisBuild.run();
           
           // Create a new worker
           copyWorker = thisBuild.createWorker();
        
           //Unbind the progress bar to whatever it was bounded to
           progressBar.progressProperty().unbind();
           
           //Set its progress to 0
           progressBar.setProgress(0);
           
           // Bind the progress bar to the copyWorkers progress
           progressBar.progressProperty().bind(copyWorker.progressProperty());
          
           // Tell the user that syncing is in progress
           progress.textProperty().bind(copyWorker.messageProperty());
           
           // Create a new thread on the copyWorker task.
           Thread copy = new Thread(copyWorker);
           
           // Start copying
           copy.start();
           
           //Once the copyworker thread succeedes
           copyWorker.setOnSucceeded(new EventHandler<WorkerStateEvent>(){
               
               //Inform the user that Syncing has completed.
               @Override
               public void handle(WorkerStateEvent t){
                   progress.setText("Syncing Completed!");
               }
           
           });

        } else {
            // if the source and target was not set then tell the user to make sure the directories are set.
            JOptionPane.showMessageDialog(null, "Please ensure that both the source and target destinations are set.", "Sync Failed", WARNING_MESSAGE);
        }
        
    }

 
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        /// LOADING AN EXISTING CONFIGURATION
        
        //Store the filename in a file
        File temp = new File("cfg.ini");
        
        // If that file exists in the current directory
        if(temp.exists()){
            
            // Try opening a scanner on it
            try{
                
                // Make a new scanner on the file
                Scanner s = new Scanner(temp);

                //Make a string array to hold the split of the string based on the "nu11" value
                String[] array = s.nextLine().split("nu11");
                
                //Set source equal to source path and show it to the user that it has been set
                source = array[0];
                srcText.setText(source);
                
                //Set target equal to target path and show it to the user that it has been set
                target = array[1];
                tarText.setText(target);
                
            }
            
            //If file failed to open throw a message to the console
            catch(FileNotFoundException ex){
                System.out.println("cfg file not found");
            }
            
            //END OF LOADING 

        }
    }
}



