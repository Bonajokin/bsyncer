/*
 * Copyright (C) 2014 Bonafide
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Copying;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.TreeMap;
import javafx.concurrent.Task;

/**
 *
 * @author Bonafide
 */
public class BuildAndCopy {
    // Strings to store the source and target paths.
    private String source = null;
    private String target = null;
    
    // Queue to hold copy threads.
    Queue<CopyThread> q = new LinkedList<>();
     
    
    // Dictionaries to prevent file overwriting.
    private final TreeMap<String,String> srcStored = new TreeMap<>();
    private final TreeMap<String,String> tarStored = new TreeMap<>();
    
    // Worker that copies all of the files to their appropriate places and updates progress.
    public Task createWorker(){
        return new Task(){
            @Override
            protected Object call() throws Exception{
                
                //Convert the queue to an array.
                Object[] Array = q.toArray();
                
                int progress = 0;
                
                for (Object Array1 : Array) {
                    //Cast the object into a copythread object and store it in the temporary variable.
                    CopyThread temp = (CopyThread) Array1;
                    this.updateMessage("Transfering: " + temp.getName());
                    //Start the current thread.
                    temp.start();
                    //wait until the current file is finished transfering before starting the next thread.
                    temp.join();
                    //update the task progress by 1 for each file its completed.
                    this.updateProgress(progress++ , Array.length);
                }
                
                return true;
            }
        
        
        };
         

}
    
    public BuildAndCopy(String src, String tar){
        source = src;
        target = tar;
    }
    
    public BuildAndCopy(){
        source = null;
        target = null;
    }
    
    public void run(){
    // If source and target are set
        if (source != null && target != null){
            
            try{
                          
                //ouput the source and target with a seperator regex and close the writer
                try ( //Create a new printwriter on the cfg
                    PrintWriter outCfg = new PrintWriter("cfg.ini")) {
                    //ouput the source and target with a seperator regex and close the writer
                    outCfg.println(source + "nu11" + target);
                }
                            
                }
            catch(FileNotFoundException ex){
                            
                System.out.println("Could not write to configuration file");
                            
            }
            
            
            //Create a src and target directory files.
            File srcDirc = new File(source);
            File tarDirc = new File(target);
            
            
            //Get the lists of files in arrays
            File[] srcFiles = srcDirc.listFiles();
            File[] tarFiles = tarDirc.listFiles();
    
           
             // Build source Dictionary   
            for(File srcFile : srcFiles){
                    if(srcFile.isDirectory()){
                        srcStored.put(srcFile.getName(), srcFile.getAbsolutePath());
                        File temp = new File(source + "\\" + srcFile.getName());
                        File[] listFiles = temp.listFiles();
                        for (File srcDirFiles : listFiles){
                            srcStored.put(srcDirFiles.getName(), srcDirFiles.getAbsolutePath());
                        }
                    } else{
                    srcStored.put(srcFile.getName(), srcFile.getAbsolutePath());
                    }
            }
            
            //Build target Dictionary
            for(File tarFile : tarFiles){
                if(tarFile.isDirectory()){
                        tarStored.put(tarFile.getName(), tarFile.getAbsolutePath());
                        File temp = new File(target + "\\" + tarFile.getName());
                        File[] listFiles = temp.listFiles();
                        for (File srcDirFiles : listFiles){
                            tarStored.put(srcDirFiles.getName(), srcDirFiles.getAbsolutePath());
                        }
                    } else{
                    tarStored.put(tarFile.getName(), tarFile.getAbsolutePath());
                    }
                
                   
            }
            
            // Loop over the source array and copy from the source to the target destination.
            for (File srcFile : srcFiles) {
               
                // Copy only if the object does not already exist in the target destination.
                    if(srcFile.isDirectory()){
                        File[] listFiles = srcFile.listFiles();

                            for (File dirFiles : listFiles){
                                if(tarStored.get(dirFiles.getName()) == null ){
                                    q.add(new CopyThread(dirFiles, target + "\\" + srcFile.getName() + "\\"));
                                } else {
                                    String temp = tarStored.get(dirFiles.getName());
                                    String temp2 = target + "\\" + srcFile.getName() + "\\" + dirFiles.getName();
                                    
                                    if(!temp.equals(temp2)){
                                    CopyThread tem = new CopyThread(dirFiles, target + "\\" + srcFile.getName() + "\\");
                                    tem.setName("Directory("+srcFile.getName()+")");
                                    q.add(tem);
                                    
                                    }
                                  }   
                                }
                            

                    } else {
                       if(tarStored.get(srcFile.getName()) ==  null){
                        //Create a new copy thread and store it in the queue.
                           CopyThread tem = new CopyThread(srcFile,target);
                           tem.setName(srcFile.getName());
                        q.add(tem); 
                        }

                    }
                }
           
    
    
        }
        
        
        
        
        
        
    }
    
    
 
}
