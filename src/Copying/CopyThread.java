/*
 * Copyright (C) 2014 Bonafide
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package Copying;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 *
 * @author Bonafide
 */
public class CopyThread extends Thread {

    private FileChannel inputChannel;
    private FileChannel outputChannel;
    private File temp;
    private final File src;
    private final String target;

   public CopyThread(File source, String tar){
        src = source;
        target = tar;
    }


    @Override
    public void run(){

        try {
            copyFile();  

        } catch(IOException ex){
            System.out.println("Failed to copy file.");
        }


    }
            
    private void copyFile() throws IOException{
        
        //Create a new input channel
        inputChannel = new FileInputStream(src).getChannel();

        //Create a new empty file in the target directory
        temp = new File(target + "\\");
        
        if(!temp.exists()){
            temp.mkdir();
        }
        
        temp = new File(target + "\\" + src.getName());
        //label.setText("Transfering " + src.getName());

        //Create a new output channel
        outputChannel = new FileOutputStream(temp).getChannel();

        //Transfer from the input channel to the output channel
        outputChannel.transferFrom(inputChannel, 0, inputChannel.size());

        //label.setText("");

        //Close both channels
        inputChannel.close();
        outputChannel.close();
    
    }
   
    
        
        
        };
    
    

